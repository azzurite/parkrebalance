using System;
using ICities;
using System.Collections.Generic;
using ColossalFramework.Plugins;
using UnityEngine;

namespace ParkRebalance {

	public class Mod : IUserMod, ILoadingExtension {
		
		private static List<BuildingEntertainmentValues> BuildingValues = new List<BuildingEntertainmentValues>{
					new BuildingEntertainmentValues("2x2_Jet_ski_rental", 170, 376),
					new BuildingEntertainmentValues("2x2_winter_fishing_pier", 60, 660),
					new BuildingEntertainmentValues("2x4_RestaurantPier", 125, 548),
					new BuildingEntertainmentValues("2x8_FishingPier", 60, 660),
					new BuildingEntertainmentValues("3x2_Fishing tours", 100, 520),
					new BuildingEntertainmentValues("4x4_Marina", 125, 560),
					new BuildingEntertainmentValues("9x15_RidingStable", 125, 528),
					new BuildingEntertainmentValues("Basketball Court", 130, 400),
					new BuildingEntertainmentValues("Beachvolley Court", 175, 312),
					new BuildingEntertainmentValues("Botanical garden", 150, 452),
					new BuildingEntertainmentValues("Cross-Country Skiing", 150, 512),
					new BuildingEntertainmentValues("Curling Park", 125, 416),
					new BuildingEntertainmentValues("Expensive Park", 75, 644),
					new BuildingEntertainmentValues("Expensive Playground", 125, 464),
					new BuildingEntertainmentValues("Expensive Plaza", 145, 400),
					new BuildingEntertainmentValues("Fishing Island", 80, 680),
					new BuildingEntertainmentValues("Floating Cafe", 75, 768),
					new BuildingEntertainmentValues("Ice Hockey Rink", 150, 380),
					new BuildingEntertainmentValues("Ice Sculpture Park", 111, 404),
					new BuildingEntertainmentValues("JapaneseGarden", 160, 316),
					new BuildingEntertainmentValues("MagickaPark", 125, 520),
					new BuildingEntertainmentValues("MerryGoRound", 125, 400),
					new BuildingEntertainmentValues("Public Firepit", 100, 468),
					new BuildingEntertainmentValues("Regular Park", 100, 504),
					new BuildingEntertainmentValues("Regular Playground", 126, 400),
					new BuildingEntertainmentValues("Regular Plaza", 110, 400),
					new BuildingEntertainmentValues("Skating Rink", 140, 364),
					new BuildingEntertainmentValues("Ski Lodge", 100, 412),
					new BuildingEntertainmentValues("Sled_Hill", 140, 376),
					new BuildingEntertainmentValues("Snowman_Park", 125, 360),
					new BuildingEntertainmentValues("Snowmobile Track", 175, 420),
					new BuildingEntertainmentValues("Tennis_Court_EU", 130, 400),
					new BuildingEntertainmentValues("Tropical Garden", 115, 500)
			};
			
		class BuildingEntertainmentValues {
			public string Name {
				get;
				private set;
			}
			
			public int Acc {
				get;
				private set;
			}
			
			public int Radius {
				get;
				private set;
			}
			
			public BuildingEntertainmentValues(string name, int acc, int radius) {
				Name = name;
				Acc = acc;
				Radius = radius;
			}
		}
	
		public string Name {
			get { return "Park Rebalance"; }
		}

		public string Description {
			get { return "Rebalances Parks to make them all worthwhile."; }
		}
		
		public void OnCreated(ILoading loading) {}
		public void OnReleased() {}
		public void OnLevelUnloading() {}
		
		public void OnLevelLoaded(LoadMode mode) {
			BuildingValues.ForEach(UpdateBuilding);
		}
		
		private void UpdateBuilding(BuildingEntertainmentValues entertainmentValues) {
				var buildingInfo = PrefabCollection<BuildingInfo>.FindLoaded(entertainmentValues.Name);
				if (buildingInfo == null) {
					Log(PluginManager.MessageType.Warning, "Could not find BuildingInfo for " + entertainmentValues.Name + "!");
					return;
				}
				
				if (buildingInfo.GetAI() == null) {
					Log(PluginManager.MessageType.Error, "buildingInfo.GetAI() for " + entertainmentValues.Name + " was null!");
					return;
				}
				
				var buildingAI = buildingInfo.GetAI() as ParkAI;
				if (buildingAI == null) {
					Log(PluginManager.MessageType.Error, "Found BuildingInfo for " + entertainmentValues.Name + " but buildingInfo.GetAI() was not an instance of ParkAI, the actual class was " + buildingInfo.GetAI().GetType().Name + "!");
					return;
				}
				buildingAI.m_entertainmentAccumulation = entertainmentValues.Acc;
				buildingAI.m_entertainmentRadius = entertainmentValues.Radius;
				
				Log(PluginManager.MessageType.Message, "The park " + entertainmentValues.Name + " successfully had its value and radius set.");
		}
		
		private void Log(PluginManager.MessageType t, string msg) {
			DebugOutputPanel.AddMessage(t, "[ParkRebalanceMod] " + msg);
			Debug.Log("[" + Enum.GetName(typeof(PluginManager.MessageType), t) + "] " + msg);
		}
	}
}
